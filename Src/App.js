// import modules
require("dotenv").config()
import Fastify from "fastify";
import Cors from "fastify-cors";
import Mongoose from "./Api//Helpers/MongooseHelper.js";
import { verifyAccessToken } from "./Api/Helpers/JwtHelper";
// instantiate app
const fastify = Fastify({ logger: true });
const port = process.env.PORT
// register modules
fastify.register(Cors, { origin: "*" });
fastify.register(Mongoose);

// routes
fastify.register(
  async (openRoutes) => openRoutes.register(require("./Api/Routes/authRoute")),
  { prefix: "/auth" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./Api/Routes/productRoutes")),
  { prefix: "/product" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./Api/Routes/categoryRoute")),
  { prefix: "/category" }
);
fastify.route({
  method: "GET",
  url: "/hey",
 preHandler: verifyAccessToken,
  handler: async (req, res) => {
    res.send({ message: "Welcome to my api" });
  },
});
// listen to app
const start = async () => {
  try {
    await fastify.ready();
    await fastify.listen(port);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
