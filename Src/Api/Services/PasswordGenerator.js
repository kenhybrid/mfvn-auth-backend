import crypto from "crypto";

const generatePassword = async () => {
    return await crypto.randomBytes(4).toString("hex");
    
};
module.exports = {
    generatePassword,
};
