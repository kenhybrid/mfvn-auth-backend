import nodemailer from "nodemailer";
require("dotenv").config();

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD,
  },
});

const sendWelcomeMail = async (data) => {
  try {
    const message = {
      from: "MEVN" + `<${process.env.EMAIL}>`,
      to: data.email,
      subject: "Wellcome to MEVN auth app",
      html: `
            <!doctype html>
                <html lang="en">
                <head>
                    <!-- Required meta tags -->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                </head>
                <body>
                    <h3>Dear ${data.username}</h3>
                    <p class="text-primary">Welcome to MEVN auth the most secure authentication platform.</p>
                    <p>Visit this link to verify your account: <a href="http://localhost:8080/auth/verify/${data.slug}">Verify</a></p>
                </body>
                </html>
      `,
      
    };
    const result = await transporter.sendMail(message);
  } catch (error) {
    console.log(error);
  }
};
const sendPassworResetMail = async (data) => {
  try {
    const message = {
      from: "MEVN" + `<${process.env.EMAIL}>`,
      to: data.email,
      subject: "Password reset for MEVN auth app",
      html: `
            <!doctype html>
                <html lang="en">
                <head>
                    <!-- Required meta tags -->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                    
                </head>
                <body>
                    <h3>Dear ${data.username}</h3>
                    <p class="text-primary">This is your new password : ${data.password} </p>
                    <p>Log in to the app and change to a convinient one.</p>
                </body>
                </html>
      `,
    };
    const result = await transporter.sendMail(message);
  } catch (error) {
    console.log(error);
  }
};
module.exports = {
  sendWelcomeMail,
  sendPassworResetMail,
};
