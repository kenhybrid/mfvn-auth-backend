const multer = require("fastify-multer");

module.exports = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, "uploads/");
      },
      filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + "-" + file.originalname);
      },
    }),
    limits: {
      fileSize: 1024 * 1024 * 5,
    },
    fileFilter: (req, file, cb) => {
      //check for file type then save
      if (
        file.mimetype === "image/jpeg" ||
        file.mimetype === "image/png" ||
        file.mimetype === "image/gif"
      ) {
        cb(null, true);
      } else {
        cb(null, false);
      }
    },
  });
