import mongoose from "mongoose";
require("dotenv").config()

export default async (fastify, options, done) => {
  try {
    await mongoose.connect(process.env.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    fastify.log.info("Database is Connected");
    done();
  } catch (error) {
    fastify.log.error("Database Error:" + error);
    done(error);
  }
};
