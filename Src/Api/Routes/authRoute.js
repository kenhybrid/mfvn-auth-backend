import {
  userLogin,
  userRegistration,
  tokenRefresh,
  logoutUser,
  passwordReset,verifyAccount
} from "../Controllers/AuthController";

const usersRoute = async (fastify, opts, done) => {
  fastify.route({
    method: "POST",
    url: "/login",
    handler: userLogin,
  });
  fastify.route({
    method: "POST",
    url: "/register",
    handler: userRegistration,
  });
  fastify.route({
    method: "POST",
    url: "/refresh",
    handler: tokenRefresh,
  });
  fastify.route({
    method: "DELETE",
    url: "/logout",
    handler: logoutUser,
  });
  fastify.route({
    method: "POST",
    url: "/reset",
    handler: passwordReset,
  });
  fastify.route({
    method: "POST",
    url: "/verify",
    handler: verifyAccount,
  });
  done();
};

module.exports = usersRoute;
