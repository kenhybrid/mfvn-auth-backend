import {
  getAllProducts,
  getOneProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  searchProduct,
  filterProduct,
} from "../Controllers/ProductsController";
import { verifyAccessToken } from "../Helpers/JwtHelper";

// routes
const productRoute = async (fastify, opts, done) => {
  fastify.route({
    method: "GET",
    url: "/all",
    // preHandler: verifyAccessToken,
    handler: getAllProducts,
  });
  fastify.route({
    method: "GET",
    url: "/one/:id",
    preHandler: verifyAccessToken,
    handler: getOneProduct,
  });
  fastify.route({
    method: "POST",
    // preHandler: verifyAccessToken,
    url: "/create",
    handler: createProduct,
  });
  fastify.route({
    method: "PUT",
    preHandler: verifyAccessToken,
    url: "/update/:id",
    handler: updateProduct,
  });
  fastify.route({
    method: "DELETE",
    preHandler: verifyAccessToken,
    url: "/delete/:id",
    handler: deleteProduct,
  });
  fastify.route({
    method: "POST",
    preHandler: verifyAccessToken,
    url: "/search",
    handler: searchProduct,
  });
  fastify.route({
    method: "POST",
    preHandler: verifyAccessToken,
    url: "/filter",
    handler: filterProduct,
  });
  //todo create a separate image uploader
  // fastify.route({
  //   method: "POST",
  //   preHandler: verifyAccessToken,
  //   url: "/upload",
  //   handler: imageUpload,
  // });
  done();
};

module.exports = productRoute;
