import {
  categoriesAll,
  categorySort,
  categoryCreate,
  categoryDelete,
  categoryUpdate,
} from "../Controllers/CategoryController";
import { verifyAccessToken } from "../Helpers/JwtHelper";

// routes
const categoryRoute = async (fastify, opts, done) => {
  fastify.route({
    method: "GET",
    url: "/all",
    // preHandler: verifyAccessToken,
    handler: categoriesAll,
  });
  fastify.route({
    method: "POST",
    url: "/sort/:id",
    preHandler: verifyAccessToken,
    handler: categorySort,
  });
  fastify.route({
    method: "POST",
    preHandler: verifyAccessToken,
    url: "/create",
    handler: categoryCreate,
  });
  fastify.route({
    method: "PUT",
    preHandler: verifyAccessToken,
    url: "/update/:id",
    handler: categoryUpdate,
  });
  fastify.route({
    method: "DELETE",
    preHandler: verifyAccessToken,
    url: "/delete/:id",
    handler: categoryDelete,
  });

  done();
};

module.exports = categoryRoute;
