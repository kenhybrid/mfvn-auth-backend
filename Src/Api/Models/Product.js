import mongoose from "mongoose";
import mongoosePaginate from 'mongoose-paginate-v2';
//creating a products schema
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    qty: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    short_description: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    unit: {
      type: String,
      required: true,
    },
    size: {
      type: String,
      required: true,
    },
    image_url: {
      type: String,
    },
    category: {
      type: String,
      required: true,
    },
    rating: {
      type: Number,
      default: "4.5",
    },
  },
  { timestamps: true }
);

productSchema.plugin(mongoosePaginate);
productSchema.index(
  { name: "text", short_description: "text" },
  { weights: { name: 10, short_description: 5 } }
);

module.exports = mongoose.model("product", productSchema);
