import mongoose from "mongoose";
import Product from "./Product";
//creating a products schema
const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    image_url: {
      type: String,
    },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: Product }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("categories", categorySchema);
