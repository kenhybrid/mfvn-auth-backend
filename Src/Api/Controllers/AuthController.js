import User from "../Models/User";
import argon from "argon2";
import { NotFound, Unauthorized, BadRequest, Conflict } from "http-errors";
import client from "../Helpers/RedisHelper";
import {
  sendWelcomeMail,
  sendPassworResetMail,
} from "../Services/MailingService.js";
import { generatePassword } from "../Services/PasswordGenerator.js";
import {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
} from "../Helpers/JwtHelper.js";
import {
  authRegisterSchema,
  authLoginSchema,
  authPasswordResetSchema,
} from "../Validations/UserValidations.js";

const userLogin = async (req, res, fastify) => {
  try {
    // console.log(req.headers.host) for mailing using local host domain
    // validation and sanitization
    const validData = await authLoginSchema.validateAsync(req.body);
    // getting a match from db
    const user = await User.findOne({ email: validData.email });
    if (!user) throw NotFound("This email is not registered!");
    if (user.verified == false)
      throw Conflict("Your account is not yet verified!");
    // comparing passwords
    const passwordMatch = await argon.verify(user.password, validData.password);
    if (!passwordMatch) throw Unauthorized("Authentication failure!");
    // generating accessToken and a refreshToken
    const id = await JSON.stringify(user._id);
    const accessToken = await signAccessToken(id);
    const refreshToken = await signRefreshToken(id);
    // sending response
    res.send({ accessToken, refreshToken });
  } catch (error) {
    if (error.isJoi === true) return BadRequest("Invalid email or password!");

    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const userRegistration = async (req, res) => {
  try {
    // validation
    const validData = await authRegisterSchema.validateAsync(req.body);
    // check if  email already exists
    const doesExist = await User.findOne({ email: validData.email });
    if (doesExist) throw Conflict("Email is already registered!");
    // if validation is true hash password
    const hashedPassword = await argon.hash(validData.password);
    if (!hashedPassword) throw BadRequest("Failed to hash password");
    // generate slug
    const slug = await generatePassword();
    // save user
    const user = await new User({
      email: validData.email,
      slug: slug,
      username: validData.username,
      password: hashedPassword,
    });
    const doc = await user.save();
    // response
    res.send({ doc });
    // SEND A WELCOME MAIL
    const mail = await sendWelcomeMail(doc);
  } catch (error) {
    if (error.isJoi === true) error.status = 422;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const verifyAccount = async (req, res) => {
  try {
    const { slug } = req.body;
    // find a user with the same slug
    const user = await User.findOne({ slug: `${slug}` });
    if (!user) throw NotFound("Seems like your account has a problem!");
    // update user acount to verified
    const updated = await User.updateOne(
      { slug: slug },
      {
        $set: {
          verified: true,
        },
      }
    );
    res.send({ message: "Account has been verified" });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const tokenRefresh = async (req, res) => {
  try {
    const { refreshToken } = req.body;
    if (!refreshToken) throw BadRequest();
    const userId = await verifyRefreshToken(refreshToken);

    const accessToken = await signAccessToken(userId);
    const refToken = await signRefreshToken(userId);
    res.send({ accessToken: accessToken, refreshToken: refToken });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const logoutUser = async (req, res, next) => {
  try {
    const { refreshToken } = req.body;
    if (!refreshToken) throw BadRequest();
    const userId = await verifyRefreshToken(refreshToken);
    client.DEL(userId, (err, val) => {
      if (err) {
        console.log(err.message);
        throw InternalServerError();
      }
      console.log(val);
      res.send({ message: "You are logged out!" });
    });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const passwordReset = async (req, res) => {
  try {
    // validate data
    const validData = await authPasswordResetSchema.validateAsync(req.body);
    // check if email is registered
    const user = await User.findOne({ email: validData.email });
    if (!user) throw NotFound(`${validData.email} is not registered!`);
    // generate random password
    const password = await generatePassword();
    // hash the password
    const hashedPassword = await argon.hash(password);
    if (!hashedPassword)
      throw createError.BadRequest("Failed to hash password");
    // update password to db
    const updated = await User.updateOne(
      { email: validData.email },
      {
        $set: {
          password: hashedPassword,
        },
      }
    );
    // send new password via email
    const data = {
      email: user.email,
      username: user.username,
      password: password,
    };
    const mail = await sendPassworResetMail(data);
    res.send({ message: "Email has been sent" });
  } catch (error) {
    // catching errors
    if (error.isJoi === true) error.status = 422;
    res.send(error);
  }
};
// todo create a change password service
module.exports = {
  userLogin,
  userRegistration,
  tokenRefresh,
  logoutUser,
  passwordReset,
  verifyAccount,
};
