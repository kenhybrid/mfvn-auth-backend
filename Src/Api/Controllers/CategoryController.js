import Category from "../Models/Category";
import Product from "../Models/Product";
import {
  categoryCreateValidation,
  categoryUpdateValidation,
} from "../Validations/categoryValidation";
import { NotFound } from "http-errors";

// contollers
const categoriesAll = async (req, res) => {
  try {
    // get data from redis cache
    // paginate data
    const doc = await Category.find()
      .select("name image_url products")
      .sort({ createdAt: "asc" });
    res.send({ doc: doc });
  } catch (error) {
    // error handling
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const categoryCreate = async (req, res) => {
  try {
    // validation
    const validData = await categoryCreateValidation.validateAsync(req.body);
    // create a category
    const category = await new Category({
      name: validData.name,
    });
    const doc = await category.save();
    // print success
    res.send({ message: "Category has been created." });
  } catch (error) {
    // error handling
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const categoryUpdate = async (req, res) => {
  try {
    const { id } = req.params;
    // validation
    const validData = await categoryUpdateValidation.validateAsync(req.body);
    // create a category
    const category = await Category.findOne({ _id: id });
    if (!category) throw NotFound("Category not found");

    const update = await Category.updateOne(
      { _id: id },
      { name: validData.name }
    );
    // print success
    res.send({ message: "Category has been updated." });
  } catch (error) {
    // error handling
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const categorySort = async (req, res) => {
  try {
      const {id} = req.params
    // get category by id and populate products
    const doc = await Category.findOne({ _id: id })
      .select("name")
      .populate("products", "name short_description size price unit");
    if (!doc) throw NotFound("Category not found");
    res.send({ doc });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const categoryDelete = async (req, res) => {
  try {
    const { id } = await req.params;
    //  find category
    const doc = await Category.findOne({ _id: id });
    if (!doc) throw NotFound("Category does not exist");
    // delete products
    if (doc.products.length >= 1) {
      const productDelete = await Product.deleteMany({
        _id: { $in: doc.products },
      });
    }
    // delete category
    const deleteCategory = await Category.deleteOne({ _id: id });
    // on success
    res.send({ message: "Category has been deleted" });
  } catch (error) {
    //  error handler
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};

module.exports = {
  categoriesAll,
  categorySort,
  categoryCreate,
  categoryDelete,
  categoryUpdate,
};
