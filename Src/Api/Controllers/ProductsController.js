import Product from "../Models/Product";
import Category from "../Models/Category";
import { BadRequest, NotFound } from "http-errors";
import client from "../Helpers/RedisHelper";
import {
  productCreateValidation,
  updateValidation,
  productSearchValidation,
  productfilterValidation,
} from "../Validations/productsValidation";

// controllers
// const getPagination = (page, size) => {
//   const limit = size ? +size : 3;
//   const offset = page ? page * limit : 0;
//
//   return { limit, offset };
// };
const getAllProducts = async (req, res) => {
  try {
    const { page } = req.query;
    const size = 12;
    const { limit, offset } = getPagination(page, size);
    // get data from redis cache
    // paginate data
    const products = await Product.paginate(
      {},
      {
        offset,
        limit,
        select:
          "name short_description price size qty unit image_url",
      }
    );
    // const products = await Product.find()
    //   .select(
    //     "name short_description description price size qty unit image_url"
    //   )
    //   .sort({ createdAt: "asc" });
    res.send({
      products: products.docs,
      pagination: {
        items: products.totalDocs,
        totalPages: products.totalPages,
        currentPage: products.page,
        next: products.nextPage < 1 ? "null" : products.nextPage - 1,
        prev: products.prevPage < 1 ? "null" : products.prevPage - 1,
      },
    });
    // res.send({
    //   products
    // });
  } catch (error) {
    // return error
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const getOneProduct = async (req, res) => {
  try {
    const { id } = req.params;
    // check for an existing product
    const doc = await Product.findOne({ _id: id }).select(
      "name description short_description price size qty unit image_url category rating"
    );
    if (!doc) throw NotFound("The product does not exist!");
    // send response
    res.send({ doc: doc });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const createProduct = async (req, res) => {
  try {
    // vadidation
    const validData = await productCreateValidation.validateAsync(req.body);
    const {
      short_description,
      name,
      description,
      price,
      qty,
      size,
      unit,
      category,
    } = validData;
    // find if category exists
    const categoryExists = await Category.findOne({ _id: category });
    if (!categoryExists) throw NotFound("The category does not exist!");
    // create a product and save
    const product = await new Product({
      name: name,
      description: description,
      short_description: short_description,
      price: price,
      qty: qty,
      unit: unit,
      size: size,
      category: category,
      image_url: null,
    });

    const doc = await product.save();
    // get the product category and push it into category products for refrence
    const categoryPopulation = await Category.updateOne(
      { _id: doc.category },
      { $push: { products: doc._id } }
    );
    // on success
    res.send({ message: "Product has been created." });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const updateProduct = async (req, res) => {
  try {
    const { id } = req.params;
    // vadidation
    const validData = await updateValidation.validateAsync(req.body);
    const {
      short_description,
      name,
      description,
      price,
      qty,
      size,
      unit,
      category,
    } = validData;

    // check for availability of the product
    const product = await Product.findById({ _id: id });
    if (!product) throw NotFound("Product dosent exist.");
    // update a product
    const update = {
      name: name || product.name,
      description: description || product.description,
      short_description: short_description || product.short_description,
      price: price || product.price,
      qty: qty || product.qty,
      size: size || product.size,
      unit: unit || product.unit,
      image_url: product.image_url, //todo rectify url
      category: category || product.category,
    };
    const updatedProduct = await Product.updateOne({ _id: id }, update);

    // update refrence in category incase category changed
    if (category) {
      if (product.category != category) {
        // remove product id from initial category
        const categoryProductRemoval = await Category.updateOne(
          { _id: product.category },
          { $pull: { products: product._id } }
        );
        // push product in new category
        const categoryPopulation = await Category.updateOne(
          { _id: category },
          { $push: { products: product._id } }
        );
      }
    }

    // print success
    res.send({ message: "Product has been updated." });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const deleteProduct = async (req, res) => {
  try {
    const { id } = req.params;
    // get product by id and pull id from category
    const product = await Product.findOne({ _id: id });
    if (!product) throw NotFound("No product with such an Id");
    //todo unlink image first
    // pull from category and update
    const category = await Category.updateOne(
      { _id: product.category },
      { $pull: { products: product._id } }
    );
    // delete product
    const deleteProduct = await Product.deleteOne({ _id: id });
    res.send({ message: "Product is deleted." });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const searchProduct = async (req, res) => {
  try {
    // validation
    const validData = await productSearchValidation.validateAsync(req.body);
    //  search a product
    const doc = await Product.find({
      $or: [
        { name: new RegExp(validData.searchTerm, "gi") },
        { short_description: new RegExp(validData.searchTerm, "gi") },
      ],
    }).select("name short_description size rating price unit image_url");
    // on success
    res.send({ doc });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const filterProduct = async (req, res) => {
  try {
    const validData = await productfilterValidation.validateAsync(req.body);
    const { unit, category, searchTerm } = validData;
    //  search and filter product
    if (!category && !unit) {
      // search without category or units
      const doc = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      }).select("name short_description size rating price unit image_url");
      // on success
      res.send({ doc });
    } else if (!category) {
      // search without category
      const doc = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("unit")
        .equals(unit)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ doc });
    } else if (!unit) {
      // search without  units
      const doc = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("category")
        .equals(category)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ doc });
    } else {
      // search with category and units
      const doc = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("category")
        .equals(category)
        .where("unit")
        .equals(unit)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ doc });
    }
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
module.exports = {
  getAllProducts,
  getOneProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  searchProduct,
  filterProduct,
};
