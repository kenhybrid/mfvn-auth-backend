import Joi from "@hapi/joi";

const authRegisterSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .max(50)
    .required(),
  username: Joi.string().max(30).required(),
  password: Joi.string()
    .trim()
    .min(4)
    .max(16)
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required(),
});

// validation schema
const authLoginSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .max(30)
    .required(),
  password: Joi.string()
    .trim()
    .min(4)
    .max(16)
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required(),
});

const authPasswordResetSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .max(30)
    .required(),
});
module.exports = {
  authRegisterSchema,
  authLoginSchema,
  authPasswordResetSchema,
};
