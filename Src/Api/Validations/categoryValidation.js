import Joi from "@hapi/joi";

const categoryCreateValidation = Joi.object({
  name: Joi.string().min(3).max(50).required(),
});

const categoryUpdateValidation = Joi.object({
  name: Joi.string().min(3).max(50),
});

module.exports = {
  categoryCreateValidation,
  categoryUpdateValidation,
};
