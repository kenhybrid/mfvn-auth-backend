import Joi from "@hapi/joi";

const productCreateValidation = Joi.object({
  name: Joi.string().min(3).max(50).required(),
  short_description: Joi.string().required(),
  description: Joi.string().required(),
  price: Joi.number().required(),
  qty: Joi.number().required(),
  unit: Joi.string().required(),
  size: Joi.string().required(),
  category: Joi.string().required(),
  // image_url: Joi.string(),
});

const updateValidation = Joi.object({
  name: Joi.string().min(3).max(50),
  short_description: Joi.string(),
  description: Joi.string(),
  price: Joi.number(),
  qty: Joi.number(),
  unit: Joi.string(),
  size: Joi.string(),
  category: Joi.string(),
  image_url: Joi.string(),
});

const productSearchValidation = Joi.object({
  searchTerm: Joi.string()
    .lowercase()
    .trim()
    .required()
    .regex(/[$\(\)<>]/, { invert: true }),
});

const productfilterValidation = Joi.object({
  searchTerm: Joi.string()
    .lowercase()
    .trim()
    .required()
    .regex(/[$\(\)<>]/, { invert: true }),
  unit: Joi.string(),
  category: Joi.string(),
});

module.exports = {
  productCreateValidation,
  updateValidation,
  productSearchValidation,
  productfilterValidation,
};
